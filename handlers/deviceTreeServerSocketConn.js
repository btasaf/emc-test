var WebSocketServer = require('websocket').server;
var usbDetect = require('usb-detection');
var usb = require('usb')

const getDObject = (device) => {
  let {idVendor,idProduct} = device.deviceDescriptor
  return {
    vid:idVendor,
    pid:idProduct,
    descriptor:device.deviceDescriptor,
    type:device.parent?"Device":"Hub",
    deviceList:[]
  }
}
const getDeviceTree = () => {
  let deviceList = usb.getDeviceList()
  let deviceTree = {}
  for(let i = 0 ; i < deviceList.length ; i++){
    let device = deviceList[i]

    let dObj = getDObject(device)
    let Hid = dObj.vid+"_"+dObj.pid;


    if(dObj.type === "Device"){
      let parentDObj = getDObject(device.parent)
      Hid = parentDObj.vid+"_"+parentDObj.pid

      if(!deviceTree[Hid])
        deviceTree[Hid] = parentDObj

      deviceTree[Hid].deviceList.push(dObj)
    }

    if(dObj.type === "Hub"&&!deviceTree[Hid]){
      deviceTree[Hid] = dObj
    }

  }

  return Object.values(deviceTree);
}
getDeviceTree()


usbDetect.startMonitoring();

exports.conn = (server) => {
  wsServer = new WebSocketServer({
      httpServer: server,
      autoAcceptConnections: false
  });


  wsServer.on('request', function(request) {

      var connection = request.accept('echo-protocol', request.origin);
      console.log('Connection accepted.');

      connection.sendUTF(JSON.stringify(getDeviceTree()));

      usbDetect.on('change', function(device) {
          setTimeout(() => {
            connection.sendUTF(JSON.stringify(getDeviceTree()));
          }, 200);
       });

      connection.on('message', function(message) {

      });
      connection.on('close', function(reasonCode, description) {
          console.log('Peer ' + connection.remoteAddress + ' disconnected.');
      });
  });
}
