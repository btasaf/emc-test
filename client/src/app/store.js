import { configureStore } from '@reduxjs/toolkit';
import deviceTreeReducer from '../features/DeviceTree/DeviceTreeSlice.js';

export const store = configureStore({
  reducer: {
    deviceTree: deviceTreeReducer,
  },
});
