import React, { useState ,useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  updateTree,
  incrementAsync,
  selectTree,
} from './DeviceTreeSlice';
import { conn } from './DeviceTreeSocketConn.js';
import styles from './DeviceTree.module.css';
import {DeviceCell} from './DeviceCell/DeviceCell.js';

export function DeviceTree() {
  const tree = useSelector(selectTree);
  const dispatch = useDispatch();
  const [viewMood,setviewMood] = useState("tree")

  useEffect(() => {
     conn((tree) => {dispatch(updateTree(tree))})
  },[]);

  return (
    <div>
      {treeDisplay(tree)}
      {toggleBtn()}
    </div>
  );

  function toggleBtn() {
    let btnProps = {
      className:styles.toggleBtn,
      onClick:() => setviewMood(viewMood === "tree"?"group":"tree")
    }
    return(
      <div>
        <button {...btnProps}>
          Mood: {viewMood}
        </button>
      </div>
    )
  }
  function deviceCell(device) {
    return(
      <DeviceCell
        device={device}
        viewMood={viewMood}
        />
    )
  }
  function treeDisplay(tree) {
    if(tree === null) return

    let display2 =  tree.reduce((acc, item) => {
      acc.push(deviceCell(item))
      if(item.type === "Hub" && item.deviceList.length > 0 && viewMood === "tree")
        item.deviceList.map(dItem => acc.push(deviceCell(dItem)))
      return acc;
    }, []);

    if(viewMood === "group"){
      //add titles
      display2 = [<p className={styles.groupTitle}>Hubs</p>].concat(display2)
      display2.push(<p className={styles.groupTitle}>Devices</p>)

      //add Devices
      display2 = display2.concat(
        tree.reduce((acc, item) => {
          if(item.type === "Hub" && item.deviceList.length > 0 )
            item.deviceList.map(dItem => acc.push(deviceCell(dItem)))
          return acc;
        }, [])
      )
    }

    return(
      <div>
        {display2}
      </div>
    )
  }
}
