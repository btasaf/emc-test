import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchCount } from './DeviceTreeAPI';

const initialState = {
  tree: null,
};



export const deviceTreeSlice = createSlice({
  name: 'deviceTree',
  initialState,
  reducers: {
    updateTree: (state,action) => {
      state.tree = action.payload;
    },
  }
});

export const { updateTree } = deviceTreeSlice.actions;

export const selectTree = (state) => state.deviceTree.tree;



export default deviceTreeSlice.reducer;
