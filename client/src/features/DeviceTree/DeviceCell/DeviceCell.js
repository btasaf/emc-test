import React,{useState} from 'react';
import styles from './DeviceCell.module.css';


export function DeviceCell(props) {
  let descriptor = []
  const [isOpen,setIsOpen] = useState(false)
  const {device,viewMood} = props
  if(device.descriptor)
    descriptor = Object.entries(device.descriptor).map(desc => <div>{desc[0]+":"+desc[1]}</div>)

  let id = device.vid+"_"+device.pid

  let cellProps = {
    className:styles.cellCon,
    style:{
      marginLeft:device.type === "Device"&&viewMood === "tree"?"50px":"20px",
      maxHeight:isOpen?"900px":"80px"
    },
    onClick:() => {
        setIsOpen(!isOpen)
    },
  }

  return (
    <div {...cellProps}>
      <div className={styles.cellHeader}>
        <div>Type:{device.type}</div>
        <div>Vid:{device.vid}</div>
        <div>Pid:{device.pid}</div>
      </div>
      <div className={styles.celldescTitle}>
        descriptor:
      </div>
      <div className={styles.celldesc}>
        {descriptor}
      </div>
    </div>
  )
}
