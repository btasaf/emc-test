var W3CWebSocket = require('websocket').w3cwebsocket;

export function conn(dispath) {

  var client = new W3CWebSocket('ws://localhost:80/', 'echo-protocol');

  client.onerror = function() {
      console.log('Connection Error');
  };

  client.onopen = function() {
      console.log('WebSocket Client Connected');
      
  };

  client.onclose = function() {
      console.log('echo-protocol Client Closed');
  };

  client.onmessage = function(e) {
    console.log(e.data);
      dispath(JSON.parse(e.data))
  };
}
