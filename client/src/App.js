import React from 'react';
import logo from './logo.svg';
import { DeviceTree } from './features/DeviceTree/DeviceTree.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <DeviceTree/>
    </div>
  );
}

export default App;
